#include "opt.h"
#include <algorithm>
#include <cstdio>
#include <set>
#include <stack>
#include <unordered_map>
#include <vector>
using std::set;
using std::stack;
using std::unordered_map;
using std::vector;

vector<Ir> optimizer::reallocate(vector<Ir> ir_list) {
  stack<uint32_t> stk;
  for (uint32_t i = 0; i < ir_list.size(); ++i) {
    if (ir_list[i].type == Ir::JZ) {
      stk.push(i);
    } else if (ir_list[i].type == Ir::JNZ) {
      uint32_t jz = stk.top();
      stk.pop();
      ir_list[jz].param = i + 1;
      ir_list[i].param = jz + 1;
    }
  }
  return ir_list;
}

vector<Ir> optimizer::offset_optimize(vector<Ir> ir_list) {
  vector<Ir> ir(ir_list);
  int i = 0;
  set<Ir::IrType> block_type = {Ir::ADD,  Ir::SUB, Ir::MVR,  Ir::MVL,
                                Ir::_OUT, Ir::_IN, Ir::LOAD, Ir::MUL};
  while (i < ir.size()) {
    while (i < ir.size() && !(block_type.count(ir[i].type)))
      ++i;
    if (i >= ir.size())
      break;
    auto j = i;
    while (j < ir.size() && block_type.count(ir[j].type))
      ++j;
    vector<Ir> instr_pack;
    unordered_map<int, vector<Ir>> offset_pack;
    vector<int> order;
    auto p = 0;
    for (int beg = i; beg < j; ++beg) {
      auto op = ir[beg];
      if (op.type == Ir::MVR)
        p += op.param;
      else if (op.type == Ir::MVL)
        p -= op.param;
      else if (set<Ir::IrType>{Ir::_IN, Ir::_OUT, Ir::LOAD}.count(op.type)) {
        vector<Ir> l;
        if (offset_pack.count(p)) {
          l = offset_pack[p];
        }
        for (auto &ii : l) {
          ii.offset = p;
        }
        vector<Ir> vec3;
        vec3.insert(vec3.end(), instr_pack.begin(), instr_pack.end());
        vec3.insert(vec3.end(), l.begin(), l.end());
        instr_pack = vec3;
        op.offset = p;
        instr_pack.push_back(op);
        offset_pack[p] = vector<Ir>();
      } else {
        if (!offset_pack.count(p)) {
          offset_pack[p] = vector<Ir>();
        }
        offset_pack[p].push_back(op);
        order.push_back(p);
      }
    }
    for (const auto &off : order) {
      auto l = vector<Ir>();
      if (offset_pack.count(off)) {
        l = offset_pack[off];
      }
      for (auto &ii : l) {
        ii.offset = off;
      }
      vector<Ir> vec3;
      vec3.insert(vec3.end(), instr_pack.begin(), instr_pack.end());
      vec3.insert(vec3.end(), l.begin(), l.end());
      instr_pack = vec3;
      offset_pack[off] = vector<Ir>();
    }
    if (p) {
      instr_pack.push_back(Ir{Ir::MVR, p, 0});
    } else if (p < 0) {
      instr_pack.push_back(Ir{Ir::MVL, -p, 0});
    }
    vector<Ir> vec3;
    auto first_end = ir.begin() + i;
    auto second_begin = ir.begin() + j;
    i += instr_pack.size() + 1;
    vec3.insert(vec3.end(), ir.begin(), first_end);
    vec3.insert(vec3.end(), instr_pack.begin(), instr_pack.end());
    vec3.insert(vec3.end(), second_begin, ir.end());
    ir = vec3;
  }
  return ir;
}

vector<Ir> optimizer::mul_optimize(vector<Ir> ir_list) {
  auto ir(ir_list);
  int i = 0;
  for (;;) {
    while (i < ir.size()) {
      if (ir[i].type == Ir::JZ)
        break;
      ++i;
    }
    if (i >= ir.size())
      break;
    auto j = i + 1;
    while (j < ir.size()) {
      if (ir[j].type == Ir::JNZ) {
        break;
      }
      if (ir[j].type == Ir::JZ) {
        i = j;
      }
      ++j;
    }
    if (j >= ir.size()) {
      break;
    }
    bool pure = true;
    for (int beg = i + 1; beg < j; ++beg) {
      auto op = ir[beg];
      if (!(op.type == Ir::ADD || op.type == Ir::MVR || op.type == Ir::MVL ||
            op.type == Ir::SUB)) {
        pure = false;
        break;
      }
    }
    if (!pure) {
      i = j;
      continue;
    }
    unordered_map<int, int> mem;
    mem[0] = 0;
    auto p = 0;
    for (int beg = i + 1; beg < j; ++beg) {
      auto op = ir[beg];
      auto type = op.type;
      if (type == Ir::ADD) {
        if (mem.count(p + op.offset)) {
          mem[p + op.offset] = mem[p + op.offset] + op.param;
        } else {
          mem[p + op.offset] = op.param;
        }
      } else if (type == Ir::SUB) {
        if (mem.count(p + op.offset)) {
          mem[p + op.offset] = mem[p + op.offset] - op.param;
        } else {
          mem[p + op.offset] = -op.param;
        }
      } else if (type == Ir::MVR) {
        p += op.param;
      } else if (type == Ir::MVL) {
        p -= op.param;
      }
    }
    if (p || mem[0] != -1) {
      i = j;
      continue;
    }
    mem.erase(0);
    vector<Ir> optblock;
    for (const auto &i : mem) {
      optblock.push_back(Ir{Ir::MUL, i.second, i.first});
    }
    vector<Ir> vec3;
    auto first_end = ir.begin() + i;
    auto second_begin = ir.begin() + j + 1;
    i += optblock.size() + 2;
    vec3.insert(vec3.end(), ir.begin(), first_end);
    vec3.insert(vec3.end(), optblock.begin(), optblock.end());
    vec3.push_back(Ir{Ir::LOAD, 0, 0});
    vec3.insert(vec3.end(), second_begin, ir.end());
    ir = vec3;
  }
  return ir;
}

vector<Ir> optimizer::fold_optimize(vector<Ir> ir_list) {
  vector<Ir> new_list;
  int state = 0;
  auto fold = Ir{};
  for (const auto &op : ir_list) {
    if (state == 0) {
      if (set<Ir::IrType>{Ir::JZ, Ir::JNZ, Ir::_IN, Ir::_OUT}.count(op.type)) {
        new_list.push_back(op);
      } else if (op.type == Ir::ADD || op.type == Ir::SUB) {
        fold = Ir{Ir::ADD, ((op.type == Ir::ADD) ? 1 : -1), 0};
        state = 1;
      } else if (op.type == Ir::MVR || op.type == Ir::MVL) {
        fold = Ir{Ir::MVR, (op.type == Ir::MVR ? 1 : -1), 0};
        state = 2;
      }
    } else if (state == 1) {
      if (op.type == Ir::ADD) {
        ++fold.param;
      } else if (op.type == Ir::SUB) {
        --fold.param;
      } else {
        if (fold.param) {
          new_list.push_back(fold);
        }
        if (op.type == Ir::MVR || op.type == Ir::MVL) {
          fold = Ir{Ir::MVR, (op.type == Ir::MVR ? 1 : -1), 0};
          state = 2;
        } else {
          state = 0;
          new_list.push_back(op);
        }
      }
    } else if (state == 2) {
      if (op.type == Ir::MVR) {
        ++fold.param;
      } else if (op.type == Ir::MVL) {
        --fold.param;
      } else {
        if (fold.param) {
          new_list.push_back(fold);
        }
        if (op.type == Ir::ADD || op.type == Ir::SUB) {
          fold = Ir{Ir::ADD, (op.type == Ir::ADD ? 1 : -1), 0};
          state = 1;
        } else {
          state = 0;
          new_list.push_back(op);
        }
      }
    }
  }
  return new_list;
}

vector<Ir> optimizer::head_loop_optimize(vector<Ir> ir_list) {
  if (ir_list.empty())
    return ir_list;
  ir_list = reallocate(ir_list);
  int i = 0;
  bool dup = false;
  if (ir_list[i].type == Ir::JZ) {
    i = ir_list[i].param;
    dup = true;
  }
  auto new_list = vector<Ir>(ir_list.begin() + i, ir_list.end());
  return new_list;
}

vector<Ir> optimizer::dead_loop_optimize(vector<Ir> ir_list) {
  vector<Ir> new_list;
  if (ir_list.empty())
    return ir_list;
  ir_list = reallocate(ir_list);
  int i = 0;
  bool dup = false;
  for (; i < ir_list.size();) {
    auto op = ir_list[i];
    if (op.type == Ir::JZ) {
      if (dup) {
        i = op.param;
      } else {
        dup = true;
        // new_list.insert(new_list.end(), ir_list.begin() + i,
        //                 ir_list.begin() + op.param);
        new_list.push_back(op);
        vector<Ir> inner(ir_list.begin() + i + 1,
                         ir_list.begin() + op.param - 1);
        auto inner_opt = dead_loop_optimize(inner);
        new_list.insert(new_list.end(), inner_opt.begin(), inner_opt.end());
        new_list.push_back(ir_list[op.param - 1]);
        i = op.param;
      }
    } else if (op.type == Ir::JNZ) {
      fprintf(stderr, "fatal error!!\n");
      exit(EXIT_FAILURE);
    } else {
      new_list.push_back(op);
      dup = false;
      ++i;
    }
  }
  return new_list;
}

vector<Ir> optimizer::load_optimize(vector<Ir> ir_list) {
  vector<Ir> new_list;
  for (int i = 0; i < ir_list.size();) {
    auto ir = ir_list[i];
    if (i + 2 < ir_list.size() && ir.type == Ir::JZ &&
        ir_list[i + 1].type == Ir::ADD && ir_list[i + 2].type == Ir::JNZ) {
      auto load_val = 0;
      if (i + 3 < ir_list.size() && ir_list[i + 3].type == Ir::ADD) {
        load_val = ir_list[i + 3].param;
        ++i;
      }
      i += 3;
      new_list.push_back(Ir{Ir::LOAD, load_val, 0});
    } else {
      new_list.push_back(ir);
      ++i;
    }
  }
  return new_list;
}

vector<Ir> optimizer::optimize() {
  return reallocate((offset_optimize(load_optimize(
      dead_loop_optimize(head_loop_optimize(fold_optimize(ir_list)))))));
}