#include "cg.h"
#include "ir.h"
code_gen::code_gen() {
  code.init(rt.environment());
  code.attach(&cc);
  cc.addFunc(FuncSignatureT<void>());
  strl.setFile(stdout);
  strl.setFlags(FormatFlags::kExplainImms);
  cc.setLogger(&strl);
}

void code_gen::add_offset(x86::Gp &gp, int offset) {
  if (offset > 0) {
    cc.add(gp, offset);
  } else if (offset < 0) {
    cc.sub(gp, -offset);
  }
}

void code_gen::add_param(x86::Mem &id, int param) {
  if (param > 0) {
    cc.add(id, param);
  } else if (param < 0) {
    cc.sub(id, -param);
  }
}

void code_gen::gen(uint32_t memsize, vector<Ir> &ir_list) {
  this->memsize = memsize;
  _stack = cc.newStack(memsize, sizeof(void *));
  sp = cc.newGpz();
  tmp = cc.newGpb();
  tmp2 = cc.newGpq();
  idx = x86::Mem(_stack);
  idx.setIndex(sp);
  idx.setSize(sizeof(uint8_t));
  // Clear the stack (memset zero)
  cc.mov(sp, 0);              // sp = 0
  Label loop = cc.newLabel(); // loop:
  cc.bind(loop);

  cc.mov(idx, 0);      // stack[i] = 0
  cc.inc(sp);          // sp++
  cc.cmp(sp, memsize); // if sp < MAX_STACK then
  cc.jb(loop);         //     goto loop
  // Clear sp by xor
  cc.xor_(sp, sp); // sp = 0
  for (int i = 0; i < ir_list.size(); ++i) {
    visit(&ir_list[i]);
  }
  finalize();
}
void code_gen::visit(Ir *ir) {
  // printf("IR<IRType.%s, %d, %d>\n", ir->to_str(), ir->param, ir->offset);
  ir->accept(this);
}
void code_gen::visit_mvl(Ir *ir) { add_offset(sp, -ir->param); }
void code_gen::visit_mvr(Ir *ir) { add_offset(sp, ir->param); }
void code_gen::visit_add(Ir *ir) {
  add_offset(sp, ir->offset);
  add_param(idx, ir->param);
  add_offset(sp, -ir->offset);
}
void code_gen::visit_sub(Ir *ir) {
  add_offset(sp, ir->offset);
  add_param(idx, -ir->param);
  add_offset(sp, -ir->offset);
}
void code_gen::visit_in(Ir *ir) {
  cc.invoke(&invk, imm(&fgetc), FuncSignatureT<int, FILE *>());
  invk->setArg(0, stdin);
  invk->setRet(0, tmp);
  add_offset(sp, ir->offset);
  cc.mov(idx, tmp);
  add_offset(sp, -ir->offset);
}
void code_gen::visit_out(Ir *ir) {
  add_offset(sp, ir->offset);
  cc.mov(tmp, idx);
  add_offset(sp, -ir->offset);
  cc.invoke(&invk, imm(&fputc), FuncSignatureT<int, int, FILE *>());
  invk->setArg(0, tmp);
  invk->setArg(1, stdout);
}
void code_gen::visit_jz(Ir *ir) {
  auto lbls = make_pair(cc.newLabel(), cc.newLabel());
  cc.bind(lbls.first);
  cc.cmp(idx, 0);
  cc.jz(lbls.second);
  jmpstack.push(lbls);
}
void code_gen::visit_jnz(Ir *ir) {
  if (jmpstack.empty()) {
    fprintf(stderr, "why it crashed here??\n");
    exit(EXIT_FAILURE);
  }
  auto lbls = jmpstack.top();
  jmpstack.pop();
  cc.jmp(lbls.first);
  cc.bind(lbls.second);
}
void code_gen::visit_load(Ir *ir) {
  add_offset(sp, ir->offset);
  cc.mov(idx, ir->param);
  add_offset(sp, -ir->offset);
}

void code_gen::visit_mul(Ir *ir) {
  int x = ir->offset;
  uint8_t y = ir->param;
  // cc.mov(x86::regs::al, y);
  // cc.mul(x86::regs::ax, idx);
  // add_offset(sp, x);
  // cc.mov(tmp, idx);
  // cc.add(tmp, x86::regs::al);
  // cc.add(idx, tmp);
  // add_offset(sp, -x);
  cc.mov(tmp, idx);
  cc.invoke(&invk, imm(&mul), FuncSignatureT<uint8_t, uint8_t, uint8_t>());
  invk->setArg(0, y);
  invk->setArg(1, tmp);
  invk->setRet(0, tmp);
  add_offset(sp, x);
  cc.add(idx, tmp);
  add_offset(sp, -x);
}

void code_gen::finalize() {
  cc.endFunc();
  auto er = cc.finalize();
  if (er != kErrorOk) {
    fprintf(stderr, "failed when generating code!! %s\n",
            DebugUtils::errorAsString(er));
    exit(EXIT_FAILURE);
  }
  if (rt.add(&product, &code) != kErrorOk) {
    fprintf(stderr, "failed when compiling!!\n");
    exit(EXIT_FAILURE);
  }
}

func code_gen::get_product() { return product; }