#include "ir.h"
#include "abs_vis.h"
#include <cstdio>
#include <cstdlib>

void Ir::accept(abstract_visitor *vis) {
  switch (type) {
  case ADD: {
    vis->visit_add(this);
  } break;
  case SUB: {
    vis->visit_sub(this);
  } break;
  case MVL: {
    vis->visit_mvl(this);
  } break;
  case MVR: {
    vis->visit_mvr(this);
  } break;
  case _IN: {
    vis->visit_in(this);
  } break;
  case _OUT: {
    vis->visit_out(this);
  } break;
  case JZ: {
    vis->visit_jz(this);
  } break;
  case JNZ: {
    vis->visit_jnz(this);
  } break;
  case LOAD: {
    vis->visit_load(this);
  } break;
  case MUL: {
    vis->visit_mul(this);
  } break;
  default: {
    fprintf(stderr, "unexpected type of ir\n");
    exit(EXIT_FAILURE);
  }
  }
}

const char *Ir::name_table[] = {"ADD", "SUB", "MVL", "MVR",  "IN",
                                "OUT", "JZ",  "JNZ", "LOAD", "MUL"};