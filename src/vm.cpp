#include "vm.h"
#include <fstream>
void Vm::interpret_run() {
  if (dir) {
    dir->run();
  }
}

void Vm::jit_run() {
  if (product) {
    product();
  } else {
    fprintf(stderr, "no product!!\n");
    exit(EXIT_FAILURE);
  }
}

Vm::Vm(const char *filepath, bool do_interpret, bool do_optimize,
       uint32_t memory_size) {
  file_path = filepath;
  cg = new code_gen;
  parse_file();
  if (do_optimize) {
    opt = new optimizer{ir_list};
    ir_list = opt->optimize();
  }
  if (do_interpret) {
    type = Interprete;
    dir = new interpreter(memory_size, ir_list);
  } else {
    type = Jit;
    cg->gen(memory_size, ir_list);
    product = cg->get_product();
  }
}

void Vm::parse_file() {
  ifstream text(file_path);
  if (text.fail()) {
    fprintf(stderr, "failed to open file %s\n", file_path);
    exit(EXIT_FAILURE);
  }
  char c;
  stack<uint32_t> stk;
  while (text >> c) {
    switch (c) {
    case '+': {
      ir_list.push_back(Ir{Ir::ADD, 1, 0});
    } break;
    case '-': {
      ir_list.push_back(Ir{Ir::SUB, 1, 0});
    } break;
    case '>': {
      ir_list.push_back(Ir{Ir::MVR, 1, 0});
    } break;
    case '<': {
      ir_list.push_back(Ir{Ir::MVL, 1, 0});
    } break;
    case ',': {
      ir_list.push_back(Ir{Ir::_IN, 1, 0});
    } break;
    case '.': {
      ir_list.push_back(Ir{Ir::_OUT, 1, 0});
    } break;
    case '[': {
      stk.push(ir_list.size());
      ir_list.push_back(Ir{Ir::JZ, 0, 0});
    } break;
    case ']': {
      if (stk.empty()) {
        fprintf(stderr, "bracket not match at %zu\n", ir_list.size());
        exit(EXIT_FAILURE);
      }
      uint32_t jz = stk.top();
      stk.pop();
      ir_list.push_back(Ir{Ir::JNZ, (int32_t)jz + 1, 0});
      ir_list[jz].param = ir_list.size();
    } break;
    default: {
      continue;
    }
    }
  }
}