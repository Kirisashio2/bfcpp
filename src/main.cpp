#include "vm.h"
#include <bits/getopt_core.h>
#include <cstdlib>
#include <getopt.h>

static void help() {
  puts("Usage: bfc /path/to/file [-i] [-r]");
  puts("use `i` flag to use interpreter");
  puts("use `r` flag to disable optimization");
  exit(EXIT_SUCCESS);
}

int main(int argc, char **argv) {
  bool do_interpret = false;
  bool do_optimize = true;
  char *file = NULL;
  char c;
  while ((c = getopt(argc, argv, ":irh")) != -1) {
    switch (c) {
    case 'i': {
      do_interpret = true;
    } break;
    case 'r': {
      do_optimize = false;
    } break;
    case 'h': {
      help();
    } break;
    case '?':
    default: {
      help();
    }
    }
  }
  if (!file && optind < argc) {
    file = argv[optind++];
  } else
    help();
  if (!file)
    help();
  auto vm = Vm::new_vm(file, do_interpret, do_optimize);
  vm->run();
}