#include "interpet.h"
#include "ir.h"
#include <cstdio>

void interpreter::visit(Ir *ir) { ir->accept(this); }
void interpreter::visit_add(Ir *ir) {
  mem[(int)dptr + ir->offset] += ir->param;
  pc += 1;
}
void interpreter::visit_sub(Ir *ir) {
  mem[(int)dptr + ir->offset] -= ir->param;
  pc += 1;
}
void interpreter::visit_mvl(Ir *ir) {
  dptr -= ir->param;
  pc += 1;
}
void interpreter::visit_mvr(Ir *ir) {
  dptr += ir->param;
  pc += 1;
}
void interpreter::visit_in(Ir *ir) {
  mem[(int)dptr + ir->offset] = getc(stdin);
  pc += 1;
}
void interpreter::visit_out(Ir *ir) {
  putc(mem[(int)dptr + ir->offset], stdout);
  pc += 1;
}
void interpreter::visit_jz(Ir *ir) {
  if (!mem[dptr])
    pc = ir->param;
  else
    pc += 1;
}
void interpreter::visit_jnz(Ir *ir) {
  if (mem[dptr])
    pc = ir->param;
  else
    pc += 1;
}

void interpreter::visit_load(Ir *ir) {
  mem[(int)dptr + ir->offset] = ir->param;
  pc += 1;
}

void interpreter::visit_mul(Ir *ir) {
  mem[(int)dptr + ir->offset] += mem[dptr] * ir->param;
  ++pc;
}

void interpreter::run() {
  while (pc < ir_list.size() && pc >= 0) {
    visit(&ir_list[pc]);
  }
}