#ifndef __BFC_IR_H__
#define __BFC_IR_H__

#include <cstdint>
struct abstract_visitor;

struct Ir {
  enum IrType { ADD, SUB, MVL, MVR, _IN, _OUT, JZ, JNZ, LOAD, MUL, END };
  const static char *name_table[END];
  void accept(abstract_visitor *vis);

  const char *to_str() const { return name_table[type]; }

  IrType type;
  int32_t param;
  int32_t offset;
};

#endif