#ifndef __BFC_ABS_VIS_H__
#define __BFC_ABS_VIS_H__

struct Ir;

struct abstract_visitor {
  virtual void visit(Ir *) = 0;
  virtual void visit_default(Ir *ir) {}
  virtual void visit_add(Ir *ir) { visit_default(ir); }
  virtual void visit_sub(Ir *ir) { visit_default(ir); }
  virtual void visit_mvl(Ir *ir) { visit_default(ir); }
  virtual void visit_mvr(Ir *ir) { visit_default(ir); }
  virtual void visit_in(Ir *ir) { visit_default(ir); }
  virtual void visit_out(Ir *ir) { visit_default(ir); }
  virtual void visit_jz(Ir *ir) { visit_default(ir); }
  virtual void visit_jnz(Ir *ir) { visit_default(ir); }
  virtual void visit_load(Ir *ir) { visit_default(ir); }
  virtual void visit_mul(Ir *ir) { visit_default(ir); }
};

#endif