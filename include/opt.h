#ifndef __BFC_OPT_H__
#define __BFC_OPT_H__

#include "ir.h"
#include <vector>
struct optimizer {
  std::vector<Ir> optimize();
  std::vector<Ir> reallocate(std::vector<Ir>);
  std::vector<Ir> offset_optimize(std::vector<Ir>);
  std::vector<Ir> mul_optimize(std::vector<Ir>);
  std::vector<Ir> fold_optimize(std::vector<Ir>);
  std::vector<Ir> head_loop_optimize(std::vector<Ir>);
  std::vector<Ir> dead_loop_optimize(std::vector<Ir>);
  std::vector<Ir> load_optimize(std::vector<Ir>);
  std::vector<Ir> ir_list;
};

#endif // OPT_H
