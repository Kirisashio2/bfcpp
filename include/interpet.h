#ifndef __BFC_INTERPRET_H__
#define __BFC_INTERPRET_H__

#include "abs_vis.h"
#include <cstdint>
#include <cstring>
#include <vector>
using std::vector;

struct interpreter : public abstract_visitor {
  void visit(Ir *) override;
  void visit_add(Ir *) override;
  void visit_sub(Ir *) override;
  void visit_mvl(Ir *) override;
  void visit_mvr(Ir *) override;
  void visit_in(Ir *) override;
  void visit_out(Ir *) override;
  void visit_jz(Ir *) override;
  void visit_jnz(Ir *) override;
  void visit_mul(Ir *) override;
  void visit_load(Ir *) override;
  void run();
  vector<Ir> &ir_list;
  uint8_t *mem;
  uint32_t pc;
  uint32_t dptr;
  interpreter(uint32_t memsize, vector<Ir> &ir_list)
      : ir_list(ir_list), mem(new uint8_t[memsize]), pc(0), dptr(0) {
    memset(mem, 0, memsize * sizeof(uint8_t));
  }
  ~interpreter() { delete[] mem; }
};

#endif