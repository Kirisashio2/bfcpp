#ifndef __BFC_CG_H__
#define __BFC_CG_H__

#include "abs_vis.h"
#include <asmjit/core/codeholder.h>
#include <asmjit/core/compiler.h>
#include <asmjit/core/errorhandler.h>
#include <asmjit/core/formatter.h>
#include <asmjit/core/func.h>
#include <asmjit/core/globals.h>
#include <asmjit/core/jitruntime.h>
#include <asmjit/core/logger.h>
#include <asmjit/core/operand.h>
#include <asmjit/x86/x86compiler.h>
#include <asmjit/x86/x86emitter.h>
#include <asmjit/x86/x86operand.h>
#include <stack>
#include <vector>

typedef void (*func)();
static inline uint8_t mul(uint8_t a, uint8_t b) { return a * b; }
using namespace asmjit;
using namespace std;
struct code_gen : public abstract_visitor {
  stack<pair<Label, Label>> jmpstack;
  JitRuntime rt;
  CodeHolder code;
  x86::Compiler cc;
  uint32_t memsize;
  x86::Mem _stack;
  func product = nullptr;
  InvokeNode *invk;
  x86::Gp sp;
  x86::Gp tmp;
  x86::Gp tmp2;
  x86::Mem idx;
  FileLogger strl;
  code_gen();
  void visit(Ir *) override;
  void visit_add(Ir *) override;
  void visit_sub(Ir *) override;
  void visit_mvl(Ir *) override;
  void visit_mvr(Ir *) override;
  void visit_in(Ir *) override;
  void visit_out(Ir *) override;
  void visit_jz(Ir *) override;
  void visit_jnz(Ir *) override;
  void visit_mul(Ir *) override;
  void visit_load(Ir *) override;
  void finalize();
  func get_product();
  void gen(uint32_t memsize, vector<Ir> &);

private:
  void add_offset(x86::Gp &gp, int offset);
  void add_param(x86::Mem &id, int param);
};

#endif // CG_H
