#ifndef __BFC_VM_H__
#define __BFC_VM_H__
#include "cg.h"
#include "interpet.h"
#include "opt.h"
#include <cstdint>

struct Vm {
  static Vm *new_vm(const char *filepath, bool do_interpret = false,
                    bool do_optimize = false, uint32_t memory_size = 4096) {
    return new Vm(filepath, do_interpret, do_optimize, memory_size);
  }

  void interpret_run();

  ~Vm() {
    if (opt)
      delete opt;
    if (dir)
      delete dir;
  }

  void jit_run();
  void run() {
    if (type == Interprete) {
      interpret_run();
    } else {
      jit_run();
    }
  }

private:
  Vm(const char *filepath, bool do_interpret, bool do_optimize,
     uint32_t memory_size);
  vector<Ir> ir_list;
  code_gen *cg;
  optimizer *opt;
  interpreter *dir;
  func product = nullptr;
  enum { Interprete, Jit } type;
  const char *file_path;
  void parse_file();
};

#endif